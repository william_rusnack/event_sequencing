module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "indent": [
            "error",
            2,
            { "VariableDeclarator": { "var": 2, "let": 2, "const": 3 } }
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "never"
        ],
        "no-inner-declarations": ["off"],
        "guard-for-in": "error",
        "no-var": "error",
        "prefer-const": ["error", {
          "destructuring": "any",
          "ignoreReadBeforeAssign": false
        }],
        "comma-dangle": ['error', "always-multiline"],
        "key-spacing": ['error'],
        "quote-props": ['error', "as-needed"],
        "eqeqeq": ['error', "always"],
        "no-plusplus": ['error', {"allowForLoopAfterthoughts": true}]
    }
}
