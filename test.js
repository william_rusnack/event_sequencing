/*eslint-disable no-global-assign, no-unused-vars, no-unused-vars */
/*globals describe, it, beforeEach, after, afterEach */

const Sequence = require('./event_sequencing.js')  // to test

const combineErrors = require('combine-errors')

const jsdom = require('jsdom')
const { JSDOM } = jsdom

const virtualConsole = new jsdom.VirtualConsole()
// virtualConsole.on('error', () => {})// console.log.bind(console))
// virtualConsole.on('warn', () => {})// console.log.bind(console))
// virtualConsole.on('info', () => {})// console.log.bind(console))
// virtualConsole.on('dir', () => {})// console.log.bind(console))

let vc_error = undefined
virtualConsole.sendTo({
  error(_, err) {
    if (vc_error !== undefined)
      throw combineErrors([
        new Error('Unhandled virtualConsole error below'),
        vc_error,
        new Error('when below error was thrown'),
        err,
      ])

    vc_error = err
  },
  // log: console.log.bind(console),
})
function check_vc_error() {
  if (vc_error !== undefined) {
    const err = vc_error
    vc_error = undefined
    throw err
  }
}


const dom = new JSDOM('<!DOCTYPE html><body></body>', {virtualConsole})
window = dom.window
const { document } = window.window
const Event = window.Event
/*eslint-enable no-global-assign */

const sinon = require('sinon')
const chai = require('chai')
const sinonChai = require('sinon-chai')
chai.should()
chai.use(sinonChai)
const { expect } = chai


describe('all tests', function() {
  afterEach(check_vc_error)
  after(window.close.bind(window))

  describe('Test once.', function() {
    it('Event only handled once (no default_target use).', function() {
      const element = document.createElement('_')
      const first =  {type: 'first', listener: sinon.spy(), event: new Event('first')},
            second = {type: 'second', listener: sinon.spy(), event: new Event('second')}

      Sequence()
        .once(first.type, first.listener, element)
        .once(second.type, second.listener, element)

      // handlers should not be called on Sequence initialization
      first.listener.should.have.not.been.called
      second.listener.should.have.not.been.called

      // second should not be called before first is called
      element.dispatchEvent(second.event)
      first.listener.should.have.not.been.called
      second.listener.should.have.not.been.called

      // first should be called but not second
      element.dispatchEvent(first.event)
      first.listener.should.have.been.calledOnce
      second.listener.should.have.not.been.called

      // first should not be called agxain
      element.dispatchEvent(first.event)
      first.listener.should.have.been.calledOnce
      second.listener.should.have.not.been.called

      // second should be called
      element.dispatchEvent(second.event)
      first.listener.should.have.been.calledOnce
      second.listener.should.have.been.calledOnce

      // neither should be called again
      element.dispatchEvent(first.event)
      element.dispatchEvent(second.event)
      first.listener.should.have.been.calledOnce
      second.listener.should.have.been.calledOnce

      // called with the correct event argument
      first.listener.should.have.been.calledWith(first.event)
      second.listener.should.have.been.calledWith(second.event)
    })

    describe('Uses corret target element.', function() {
      it('Listens to default_target.', function() {
        const default_target = document.createElement('_')
        const event =  {type: 'event', listener: sinon.spy(), event: new Event('event')}

        Sequence(default_target).once(event.type, event.listener)

        default_target.dispatchEvent(event.event)
        default_target.dispatchEvent(event.event)  // not a duplicate

        event.listener.should.have.been.calledOnce
        event.listener.should.have.been.calledWith(event.event)
      })

      it('Listens to target even if default_target is defined.', function() {
        const default_target = document.createElement('_')
        const target = document.createElement('_')
        const event =  {type: 'event', listener: sinon.spy(), event: new Event('event')}

        Sequence(default_target).once(event.type, event.listener, target)

        default_target.dispatchEvent(event.event)
        default_target.dispatchEvent(event.event)  // not a duplicate
        target.dispatchEvent(event.event)
        target.dispatchEvent(event.event)  // not a duplicate

        event.listener.should.have.been.calledOnce
        event.listener.should.have.been.calledWith(event.event)
      })

      it('Throws an error is no target set.', function() {
        const event =  {type: 'event', listener: sinon.spy(), event: new Event('event')}

        expect(() =>{Sequence().once(event.type, event.listener)})
          .to.throw('No target or default target defined.')

      })
    })

  })

  describe('Test repeat.', function() {
    it('Should call listener everytime event is heard.', function() {
      const element = document.createElement('_')
      const event =  {type: 'event', listener: sinon.spy(), event: new Event('event')}

      Sequence().repeat(event.type, event.listener, element)

      // 3 events dispatched
      element.dispatchEvent(event.event)
      element.dispatchEvent(event.event)
      element.dispatchEvent(event.event)

      event.listener.should.have.been.calledThrice
    })

    it('Repeat until.', function() {
      const element = document.createElement('_')
      const repeat_event =  {type: 'repeat_event', listener: sinon.spy(), event: new Event('repeat_event')}
      const stop_event =  {type: 'stop_event', listener: sinon.spy(), event: new Event('stop_event')}

      Sequence(element)
        .repeat(repeat_event.type, repeat_event.listener)
        .until.once(stop_event.type, stop_event.listener)

      // repeat calling event
      element.dispatchEvent(repeat_event.event)
      element.dispatchEvent(repeat_event.event)
      repeat_event.listener.should.have.been.calledTwice
      repeat_event.listener.should.always.have.been.calledWithExactly(repeat_event.event)

      // call stop event
      element.dispatchEvent(stop_event.event)
      stop_event.listener.should.have.been.calledOnce
      stop_event.listener.should.have.been.calledWith(stop_event.event)

      // repeat listener should not be called again
      element.dispatchEvent(repeat_event.event)
      repeat_event.listener.should.have.been.calledTwice
    })
  })

  describe('Test until.event and wait_until.', function() {
    it('wait_until event happens before setting next listener.', function() {
      const element = document.createElement('_')
      const wait = {type: 'wait', event: new Event('wait')}
      const after = {type: 'after', listener: sinon.spy(), event: new Event('after')}

      Sequence(element)
        .wait_until(wait.type)
        .once(after.type, after.listener)

      // should wait until wait event
      element.dispatchEvent(after.event)
      after.listener.should.not.have.been.called

      // should not call after listener
      element.dispatchEvent(wait.event)
      after.listener.should.not.have.been.called

      // nothing else should happen
      element.dispatchEvent(after.event)
      element.dispatchEvent(after.event)
      after.listener.should.have.been.calledOnce
    })

    it('until.event should stop repeat.', function() {
      const element = document.createElement('_')
      const rep = {type: 'rep', listener: sinon.spy(), event: new Event('rep')}
      const ue = {type: 'ue', event: new Event('ue')}

      Sequence(element)
        .repeat(rep.type, rep.listener)
        .until.event(ue.type)

      // repeat works
      element.dispatchEvent(rep.event)
      rep.listener.should.have.been.calledOnce

      // cancle repeat
      element.dispatchEvent(ue.event)
      element.dispatchEvent(rep.event)
      rep.listener.should.have.been.calledOnce
    })
  })

  describe('whenever', function() {
    /*eslint-disable no-unused-vars */
    let element1, element2, one, two, three, restart
    /*eslint-enable no-unused-vars */
    beforeEach(function() {
      element1 = document.createElement('_')
      element2 = document.createElement('_')
      one   = {type: 'one'  , listener: sinon.spy(), event: new Event('one'  )}
      two   = {type: 'two'  , listener: sinon.spy(), event: new Event('two'  )}
      three = {type: 'three', listener: sinon.spy(), event: new Event('three')}

      restart = {type: 'restart event', event: new Event('restart event')}
    })

    describe('restart', function() {
      it('returns Sequence instance', function() {
        const seq_org = Sequence(element1)
        const seq_res = seq_org.whenever(restart.type).restart()

        expect(seq_res).to.equal(seq_org)
      })

      it('on init element', function() {
        Sequence(element1)
          .once(one.type, one.listener)
          .once(two.type, two.listener)
          .whenever(restart.type).restart()
        one.listener.should.not.have.been.called
        two.listener.should.not.have.been.called

        element1.dispatchEvent(restart.event)
        one.listener.should.not.have.been.called
        two.listener.should.not.have.been.called

        element1.dispatchEvent(one.event)
        element1.dispatchEvent(restart.event)
        one.listener.should.have.been.calledOnce
        two.listener.should.not.have.been.called

        element1.dispatchEvent(two.event)
        element1.dispatchEvent(one.event)
        element1.dispatchEvent(restart.event)
        one.listener.should.have.been.calledTwice
        two.listener.should.not.have.been.called

        element1.dispatchEvent(one.event)
        element1.dispatchEvent(two.event)
        element1.dispatchEvent(restart.event)
        one.listener.should.have.been.calledThrice
        two.listener.should.have.been.calledOnce

        element1.dispatchEvent(one.event)
        element1.dispatchEvent(two.event)
        element1.dispatchEvent(restart.event)
        one.listener.should.have.callCount(4)
        two.listener.should.have.been.calledTwice
      })

      it('on other element', function() {
        Sequence(element1)
          .once(one.type, one.listener)
          .repeat(two.type, two.listener)
          .whenever(restart.type, element2).restart()

        element1.dispatchEvent(one.event)
        element1.dispatchEvent(two.event)
        element1.dispatchEvent(two.event)
        element2.dispatchEvent(restart.event)
        element1.dispatchEvent(one.event)

        two.listener.should.have.been.calledTwice
        one.listener.should.have.been.calledTwice
      })
    })

    describe('call', function() {
      it('returns Sequence instance', function() {
        const seq_org = Sequence()
        const seq_res = seq_org.whenever(one.type, element1).call()

        expect(seq_res).to.equal(seq_org)
      })

      it('runs call listener', function() {
        const listener = sinon.spy()
        Sequence().whenever(one.type, element1).call(listener)

        element1.dispatchEvent(one.event)
        element1.dispatchEvent(one.event) // not a duplicate

        listener.should.have.been.calledTwice
      })
    })
  })

  describe('Test intended functionality.', function() {
    it('once, repeat, until.once, cycle.', function() {
      const element1 = document.createElement('_'),
            element2 = document.createElement('_')
      const one   = {type: 'one'  , listener: sinon.spy(), event: new Event('one'  )},
            two   = {type: 'two'  , listener: sinon.spy(), event: new Event('two'  )},
            three = {type: 'three', listener: sinon.spy(), event: new Event('three')}

      Sequence(element1)
        .once(one.type, one.listener)
        .repeat(two.type, two.listener)
        .until.once(three.type, three.listener, element2)
        .cycle()

      // no skipping
      element1.dispatchEvent(two.event)
      element2.dispatchEvent(three.event)

      // no cross matching of events
      element1.dispatchEvent(two.event)
      element1.dispatchEvent(three.event)
      element2.dispatchEvent(one.event)
      element2.dispatchEvent(two.event)

      // none should have been called
      one.listener.should.not.have.been.called
      two.listener.should.not.have.been.called
      three.listener.should.not.have.been.called

      const num_cycles = 3
      for (let cycle = 1; cycle <= num_cycles; ++cycle) {
        // first once
        element1.dispatchEvent(one.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * (cycle - 1))
        three.listener.should.have.callCount(cycle - 1)

        // first once does not repeat
        element1.dispatchEvent(one.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * (cycle - 1))
        three.listener.should.have.callCount(cycle - 1)

        // repeat
        element1.dispatchEvent(two.event)
        element1.dispatchEvent(two.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * cycle)
        three.listener.should.have.callCount(cycle - 1)

        // until.once stop repeat
        element2.dispatchEvent(three.event)
        element1.dispatchEvent(two.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * cycle)
        three.listener.should.have.callCount(cycle)
      }
    })

    it('once, repeat, until.event, cycle.', function() {
      const element1 = document.createElement('_'),
            element2 = document.createElement('_')
      const one   = {type: 'one'  , listener: sinon.spy(), event: new Event('one'  )},
            two   = {type: 'two'  , listener: sinon.spy(), event: new Event('two'  )},
            three = {type: 'three', event: new Event('three')}

      Sequence(element1)
        .once(one.type, one.listener)
        .repeat(two.type, two.listener)
        .until.event(three.type, element2)
        .cycle()

      // no skipping
      element1.dispatchEvent(two.event)
      element2.dispatchEvent(three.event)

      // no cross matching of events
      element1.dispatchEvent(two.event)
      element1.dispatchEvent(three.event)
      element2.dispatchEvent(one.event)
      element2.dispatchEvent(two.event)

      // none should have been called
      one.listener.should.not.have.been.called
      two.listener.should.not.have.been.called

      const num_cycles = 3
      for (let cycle = 1; cycle <= num_cycles; ++cycle) {
        // first once
        element1.dispatchEvent(one.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * (cycle - 1))

        // first once does not repeat
        element1.dispatchEvent(one.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * (cycle - 1))

        // repeat
        element1.dispatchEvent(two.event)
        element1.dispatchEvent(two.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * cycle)



        // until.event stop repeat
        element2.dispatchEvent(three.event)
        element1.dispatchEvent(two.event)
        one.listener.should.have.callCount(cycle)
        two.listener.should.have.callCount(2 * cycle)
      }
    })
  })
})

