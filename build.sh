#!/bin/bash

source_file=event_sequencing.js
built_file_min=event_sequencing.min.js
tmp_file=tmp_file

npx eslint -c .eslintrc.js $source_file && echo "eslint complete" && \
npm test && echo "Testing complete"  && \
sed 's/module\.exports/var Sequence/g' $source_file > $tmp_file && echo "Converted to browser file"
npx babel $tmp_file -o $tmp_file && echo "babel complete" && \
npx uglifyjs $tmp_file -o $built_file_min && echo "uglify complete" && \

rm $tmp_file
