# event_sequencing
Allows dom events to be sequenced on a specified order.  

![](https://github.com/BebeSparkelSparkel/event_sequencing/blob/master/flight-1179587_1280.jpg?raw=true)

## Include File
event_sequencing.min.js  
`<script src="/event_sequencing.min.js"></script>`

## API

### Initialize
**Sequence**([default_target])  
Creates a new event sequencing instance  
default_target (optional) - HTML Element that the listeners will be attached to if there is not a target included.  

### Add Listeners
**Sequence.once**(type, listener[, target])  
Sets a listener to only be called once.  
           
**Sequence.repeat**(type, listener[, target])  
Sets a listener to repeat.  
Look at *until* below to stop event from repeating.  

#### Input Arguments
**type** - String specifying the event type.  
**listener** - function(event) that will be called when the event is heard.  
             *event* is the one argument passed to the handler.  
**target** - HTML Element that the listeners will be attached to.  
           Overrides default_target  

#### Example once
```javascript
const button = document.querySelector('button')

Sequence(button)
  .once('click', event => console.log('first'))
  .once('click', event => console.log('second'))

button.dispatchEvent('click')  // 'first' is logged to console
button.dispatchEvent('click')  // 'second' is logged to console
button.dispatchEvent('click')  // nothing is logged to console
```

✮✮✮✮✮✮✮✮✮✮✮✮ LIKE IT? STAR IT! ✮✮✮✮✮✮✮✮✮✮✮✮  

#### Example once repeat
```javascript
Sequence(button)
  .once('click', event => console.log('first'))
  .repeat('click', event => console.log('repeat second'))

button.dispatchEvent(new Event('click'))  // 'first' is logged to console
button.dispatchEvent(new Event('click'))  // 'repeat second' is logged to console
button.dispatchEvent(new Event('click'))  // 'repeat second' is logged to console again
```

### until
**Sequence.until**.<method>(type, listener[, target])  
Allows the last listener to continue until event type is heard on target.  
Once the until event is heard the last event listener is removed.  

Included until methods:  
Sequence.until.once  
Sequence.until.repeat  

#### Example repeat until
```javascript
const input = document.querySelector('input[name="example"]')
const submit = document.querySelector('input[type="submit"]')

Sequence()
  .repeat('change', event => console.log('input change'), input)
  .until.once('click', event => console.log('submitted'), submit)

input.dispatchEvent(new Event('change'))  // 'input change' is logged to console
input.dispatchEvent(new Event('change'))  // 'input change' is logged to console again
submit.dispatchEvent(new Event('click'))  // 'submitted' is logged to console (disables input click listener)
input.dispatchEvent(new Event('change'))  // nothing is logged
```

### cycle
**Sequence.cycle**()  
Allows the Sequence to be cycled/restarted once the end is reached.  

#### Example cycle
```javascript
const button = document.querySelector('button')

Sequence(button)
  .once('click', event => console.log('first'))
  .once('click', event => console.log('second'))
  .cycle()
  
button.dispatchEvent('click')  // 'first' is logged to console
button.dispatchEvent('click')  // 'second' is logged to console (sequence is restarted)

button.dispatchEvent('click')  // nothing is logged to console
button.dispatchEvent('click')  // 'second' is logged to console (sequence is restarted)

...  // will cycle forever
```

### whenever
Sequence.whenever(type[,target]).**restart**()  
Will restart the sequence from the beginning when type is heard on target/default_target.  

Sequence.whenever(type[,target]).**call**(listener)  
Will call the listener funtion when type is heard on target/default_target.  

## Test and Build
`npm test`  
`npm run-script build`  

✮✮✮✮✮✮✮✮✮✮✮✮ LIKE IT? STAR IT! ✮✮✮✮✮✮✮✮✮✮✮✮  
