'use strict'

module.exports = (function() {
  function constructor(default_target) {
    /*
    Constructs a new instance of sequence

    default_target (optional) - will be used if a target isn't passed to the chain methods
      if not defined the first target will be set as the default target

    A seperate function because typescript doesn't allow auto-instantiation.
    reference: http://raganwald.com/2014/07/09/javascript-constructor-problem.html
    */
    return new sequence(default_target)
  }
  function sequence(default_target) {
    this.default_target = default_target  // may be undefined
    this.listener_queue = []  // listener queue
    this.queue_index = -1  // queue index

    this._cycle = false  // if true the sequence will repeat
  }


  sequence.prototype.cycle = function() {
    /*
    If called the defined listeners will be repeated again
    when all have been used.
    */
    if (this.listener_queue.length < 1) {
      throw new Error('Cannot cycle because there are no listeners to cycle.')
    }

    this._cycle = true

    return this
  }

  // primary methods that will allow wrappers to be put on them
  const _setup_listener = {
    once(listener) {
      /* Call listener once for the event type for target element. */

      return function(event) {
        /* wraps listener to remove it after it is called once */
        listener(event)
        this._remove_listener()
        this._next()  // tells sequence instance that the next listener should be initialized
      }
    },

    repeat(listener) {
      /*
      Allows the listener to be repeated multiple times.

      Use the .until.(once|repeat) after this to stop listener from repeating
      */
      return listener
    },
  }

  // add _setup_listener methods to sequence
  for (const sl in _setup_listener) { if (_setup_listener.hasOwnProperty(sl)) {
    const method = function(type, listener, target) {
      /* */
      target = this._find_target(target)
      listener = _setup_listener[sl](listener).bind(this)

      this._add({type, target, listener})

      return this
    }

    sequence.prototype[sl] = method
  }}

  sequence.prototype.wait_until = function(type, target) {
    // wait until event

    this._add({
      type,
      target: this._find_target(target),
      listener: () => this._next(),
    })

    return this
  }


  // Adds until methods
  // adds a wrapper that will remove the last defined event listener
  Object.defineProperty(sequence.prototype, 'until', {
    get() {
      // IMPROVEMENT: it would be great if this didn't have to recreate the methods every time

      const last_listener_index = this.listener_queue.length - 1

      const out_methods = {
        event: (type, target) => {
          // wait until event
          this._add(
            {
              type,
              target: this._find_target(target),
              listener: () => {
                this._remove_listener(last_listener_index)
                this._next()
              },
            },
            true
          )

          return this
        },
      }

      for (const sl in _setup_listener) {
        if (_setup_listener.hasOwnProperty(sl)) {
          out_methods[sl] = (type, listener, target) => {
            const bound_listener = _setup_listener[sl](listener).bind(this)

            this._add(
              {
                type,
                target: this._find_target(target),
                listener: (event) => {
                  this._remove_listener(last_listener_index)
                  bound_listener(event)
                },
              },
              true
            )

            return this
          }
        }
      }

      return out_methods
    },
  })

  sequence.prototype.whenever = function(event, target) {
    target = this._find_target(target)

    return {
      restart: () => {
        target.addEventListener(event, () => {
          if (this.queue_index < this.listener_queue.length)
            this._remove_listener()
          this.queue_index = -1
          this._next()
        })

        return this
      },

      call: listener => {
        target.addEventListener(event, listener)
        return this
      },
    }
  }

  sequence.prototype._add = function(another, launch_with_last=false) {
    /*
    Adds another listener to the listener_queue.
    If the listener_queue is empty when called the listener is set to
    listen on the target.

    another - object with the keys
      type - string that specifies the event type
             Reference: Parameters type @
              https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
      listener - function that will be called on hearing the event
                 Reference: Parameters listener @
                  https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
      target - element that the listener will be applied to
               Reference: https: //developer.mozilla.org/en-US/docs/Web/API/EventTarget
    launch_with_last (optional) - If truthy another will be set to listen with
                                  the last listener.
    */
    const num_listeners = this.listener_queue.length

    if (launch_with_last && num_listeners > 0) {
      const last_index = num_listeners - 1
      this.listener_queue[last_index].push(another)

      // launch this if adding to the current listening listeners
      if (last_index === this.queue_index) this._next(another)

    } else if (launch_with_last) {
      throw new Error('No previous listeners to launch with.')

    } else {
      this.listener_queue.push([another])

      // if initial insert start the first listener
      if (this.listener_queue.length === 1) this._next()
    }
  }

  sequence.prototype._next = function(launch) {
    /*
    Adds the next set of listeners to their targets.

    launch (optional) - Add the listener in launch instead of going
                        to the next set of listeners.
    */
    if (launch) set_listener(launch)

    else {
      this.queue_index += 1
      const queue_index = this.queue_index
      const queue_length = this.listener_queue.length

      if (queue_index < queue_length) {
        this.listener_queue[queue_index].forEach(set_listener)

      } else if (this._cycle) {
        this.queue_index = -1
        this._next()
      }
    }

    function set_listener(settings) {
      const {type, listener, target} = settings
      target.addEventListener(type, listener)
    }
  }

  sequence.prototype._remove_listener = function(queue_index) {
    /*
    Removes listeners

    queue_index (optional) - If defined removes listeners in this.listener_queue at queue_index.
                             Else removes the listeners at this.queue_index (current active listeners)
    Removes the listener that is specified in this.listener_queue at queue_index.
    If queue_index is undefined it is set to this.queue_index.
    */

    // defaults to the current this.queue_index
    if (queue_index === undefined) queue_index = this.queue_index

    // check if valid index
    const queue_length = this.listener_queue.length
    if (queue_length === 0) {
      throw new Error('listener_queue is empty.')
    }
    if (queue_index < 0 || queue_index >= queue_length) {
      throw new Error(`queue_index out of range. queue_index=${queue_index}, listener_queue.length=${queue_length}.`)
    }

    this.listener_queue[queue_index].forEach(settings => {
      const {type, listener, target} = settings
      target.removeEventListener(type, listener)
    })
  }

  sequence.prototype._find_target = function(target) {
    /*
    Determines what target element to use.

    Throws error if both target and this.default_target are undefined.
    */
    if (target !== undefined) return target
    if (this.default_target !== undefined) return this.default_target
    throw new Error('No target or default target defined.')
  }

  return constructor
})()
